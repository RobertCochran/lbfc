;; The MIT License
;;
;; Copyright 2017 Robert Cochran <robert-git@cochranmail.com>
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy of
;; this software and associated documentation files (the "Software"), to deal in
;; the Software without restriction, including without limitation the rights to
;; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
;; the Software, and to permit persons to whom the Software is furnished to do so,
;; subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
;; FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
;; COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
;; IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(defparameter *loop-start-labels* nil)
(defparameter *loop-end-labels* nil)
(defparameter *loop-number* 0)

(defmacro string-case (var &body cases)
  (let ((var-sym (gensym)))
    `(let ((,var-sym ,var))
       (cond
         ,@(loop for c in cases collecting
              `(,(if (listp (car c))
                    `(member ,var-sym ,(car c) :test #'string=)
                    `(string= ,var-sym ,(car c)))
                 ,@(cdr c)))))))

(defun gen-label ()
  (prog1
      (format nil "L~a" *loop-number*)
    (incf *loop-number*)))

(defmacro with-labels ((&rest labels) &body body)
  `(let ,(loop for l in labels collecting `(,l (gen-label)))
     ,@body))

(defun emit-commands (file)
  (with-open-file (in-stream file :if-does-not-exist :error)
    (let ((filedata (make-string (file-length in-stream))))
      (read-sequence filedata in-stream)
      (remove nil
              (loop for c across filedata collecting
                   (string-case c
                     (">" (list 'next 1))
                     ("<" (list 'prev 1))
                     ("+" (list 'add 1))
                     ("-" (list 'sub 1))
                     ("." (list 'out))
                     ("," (list 'in))
                     ("[" (list 'start))
                     ("]" (list 'end))))))))

(defun optimize-commands (commands)
  (let (optimized-commands)
    (loop for c in commands do
         (case (car c)
           ((add sub next prev)
            (if (eq (car c) (caar optimized-commands))
                (incf (cadar optimized-commands))
                (push c optimized-commands)))
           (otherwise (push c optimized-commands))))
    (reverse optimized-commands)))

(defun emit-code-for-commands (commands)
  (loop for c in commands do
       (case (car c)
         (next (format t "	addl $~d, %ebp~%" (cadr c)))
         (prev (format t "	subl $~d, %ebp~%" (cadr c)))
         (add (if (= (cadr c) 1)
                  (format t "	incb (%ebp)~%")
                  (format t "	addb $~d, (%ebp)~%" (cadr c))))
         (sub (if (= (cadr c) 1)
                  (format t "	decb (%ebp)~%")
                  (format t "	subb $~d, (%ebp)~%" (cadr c))))
         (out (format t "	movb $4, %al~%")
              (format t "	movb $1, %bl~%")
              (format t "	movl %ebp, %ecx~%")
              (format t "	movb $1, %dl~%")
              (format t "	int $0x80~%"))
         (in (format t "	movb $3, %al~%")
             (format t "	movb $0, %bl~%")
             (format t "	movl %ebp, %ecx~%")
             (format t "	movb $1, %dl~%")
             (format t "	int $0x80~%"))
         (start (with-labels (start end)
                  (push start *loop-start-labels*)
                  (push end *loop-end-labels*)
                  (format t "	jz .~a~%" end)
                  (format t ".~a:~%" start)))
         (end (format t "	jnz .~a~%" (pop *loop-start-labels*))
              (format t ".~a:~%" (pop *loop-end-labels*))))))

(defun emit-program (filename)
  (format t ".section .bss~%")
  (format t "bf_cells:~%")
  (format t "	.zero 65536~%")
  (format t ".section .text~%")
  (format t ".globl _start~%")
  (format t "_start:~%")
  (format t "	movl $bf_cells, %ebp~%")
  (emit-code-for-commands (optimize-commands (emit-commands filename)))
  ; exit() system call
  (format t "	movb $1, %al~%")
  (format t "	movb $0, %bl~%")
  (format t "	int $0x80~%"))

(defun get-argv ()
  (or #+sbcl (cdr sb-ext:*posix-argv*)
      #+ecl ext:argv
      (error "Don't know how to get C argv on this Lisp!")))

(defun write-output-to-file (file thunk)
  (with-open-file (*standard-output* file
                                     :direction :output
                                     :if-exists :supersede
                                     :if-does-not-exist :create)
    (funcall thunk)))

(let* ((argv (get-argv))
       (infile (first argv))
       (outfile (second argv)))
  (write-output-to-file outfile (lambda () (emit-program infile))))
